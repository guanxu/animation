//
//  CAAnimationVC.h
//  animation
//
//  Created by 关旭 on 2019/3/7.
//  Copyright © 2019 guanxu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CAAnimationVC : UIViewController

@property (nonatomic,assign)NSInteger animationIndex;

@end

NS_ASSUME_NONNULL_END
