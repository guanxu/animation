//
//  AnimationViewController.h
//  animation
//
//  Created by 关旭 on 2019/3/5.
//  Copyright © 2019 guanxu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AnimationViewController : UIViewController

@property (nonatomic,assign)NSInteger viewAnimationIndex;

@end

NS_ASSUME_NONNULL_END
