//
//  CAAnimationVC.m
//  animation
//
//  Created by 关旭 on 2019/3/7.
//  Copyright © 2019 guanxu. All rights reserved.
//

#import "CAAnimationVC.h"
#define SCREEN_W self.view.frame.size.width
#define SCREEN_H self.view.frame.size.height

@interface CAAnimationVC ()<CAAnimationDelegate>
@property (nonatomic,strong)CALayer *mainLayer;
@end

@implementation CAAnimationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self initView];
}

- (void)initView {
    CALayer *layer = [[CALayer alloc]init];
    layer.bounds = CGRectMake(0, 0, 100, 100);
    layer.position = self.view.center;
    layer.backgroundColor = [UIColor redColor].CGColor;
    [self.view.layer addSublayer:layer];
    
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_W/2-100, SCREEN_H-70, 200, 40)];
    [button setTitle:@"开始动画" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(startAnimation) forControlEvents:UIControlEventTouchUpInside];
    button.layer.borderColor = [UIColor grayColor].CGColor;
    button.layer.cornerRadius = 20;
    button.layer.borderWidth = 1;
    [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [self.view addSubview:button];
    
    _mainLayer = layer;
}
- (void)startAnimation {
    switch (self.animationIndex) {
        case 1://spring animation
            [self initSpringAnimation];
            break;
        case 2://CAKeyframeAnimation(晃动）
            [self initKeyframeAnimation];
            break;
        case 3://CAKeyframeAnimation(曲线位移）
            [self initKeyframeAnimation];
            break;
        case 4://转场动画
            [self initCATransitionAnimation];
            break;
        case 5://动画组
            [self initAnimationGroup];
            break;
        default:
            break;
    }
}

#pragma mark ----------------CAAnimationDelegate
- (void)animationDidStart:(CAAnimation *)anim {
    
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    
}


- (void)initAnimationGroup {
    //晃动动画
    CAKeyframeAnimation *keyFrameAni = [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation"];
    keyFrameAni.values = @[@(-(4) / 180.0*M_PI),@((4) / 180.0*M_PI),@(-(4) / 180.0*M_PI)];
    //每一个动画可以单独设置时间和重复次数,在动画组的时间基础上,控制单动画的效果
    keyFrameAni.duration = 0.3;
    keyFrameAni.repeatCount= MAXFLOAT;
    keyFrameAni.delegate = self;
//    keyFrameAni.removedOnCompletion = NO;
//    keyFrameAni.fillMode = kCAFillModeForwards;
    //位移动画
    CABasicAnimation *basicAni = [CABasicAnimation animationWithKeyPath:@"position"];
    //到达位置
    basicAni.byValue = [NSValue valueWithCGPoint:CGPointMake(100, 100)];
    //
    basicAni.duration = 1;
    basicAni.repeatCount = 1;
    //
    basicAni.removedOnCompletion = NO;
    basicAni.fillMode = kCAFillModeForwards;
    //设置代理
    basicAni.delegate = self;
    //动画时间
    basicAni.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    CAAnimationGroup *aniGroup = [CAAnimationGroup animation];
    aniGroup.animations = @[keyFrameAni,basicAni];
    aniGroup.autoreverses = YES;
    //动画的表现时间和重复次数由动画组设置的决定
    aniGroup.duration = 2;
    aniGroup.repeatCount= 3;
    //使动画结束后停留在结束位置
//    aniGroup.autoreverses = NO;
//    aniGroup.removedOnCompletion = NO;
//    aniGroup.fillMode = kCAFillModeForwards;
    //
    [_mainLayer addAnimation:aniGroup forKey:@"groupAnimation"];
    
}
- (void)initCATransitionAnimation {
    CATransition *transition = [CATransition animation];
    transition.type = @"rippleEffect";
    transition.subtype = kCATransitionFromLeft;
    transition.duration = 1;
    _mainLayer.contents = (__bridge id _Nullable)([UIImage imageNamed:@"example"].CGImage);
    [_mainLayer addAnimation:transition forKey:@"transtion"];
}

- (void)initKeyframeAnimation {
    CAKeyframeAnimation *animation = nil;
    if (self.animationIndex == 2) {//晃动
        animation = [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation"];
        animation.duration = 0.3;
        animation.values = @[@(-(4) / 180.0*M_PI),@((4) / 180.0*M_PI),@(-(4) / 180.0*M_PI)];
        animation.repeatCount=MAXFLOAT;
    }else {//曲线位移
        animation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
        UIBezierPath *path = [UIBezierPath bezierPath];
        [path moveToPoint:_mainLayer.position];
        [path addCurveToPoint:CGPointMake(300, 500) controlPoint1:CGPointMake(100, 400) controlPoint2:CGPointMake(300, 450)];
        animation.path = path.CGPath;
        animation.duration = 1;
        
        animation.removedOnCompletion = NO;
        animation.fillMode = kCAFillModeForwards;
    }
    [_mainLayer addAnimation:animation forKey:@"keyFrameAnimation"];
}


#pragma mark -----------------------CASpringAniamtion 
- (void)initSpringAnimation {
    CASpringAnimation *springAni = [CASpringAnimation animationWithKeyPath:@"position"];
    springAni.damping = 2;
    springAni.stiffness = 50;
    springAni.mass = 1;
    springAni.initialVelocity = 10;
    springAni.toValue = [NSValue valueWithCGPoint:CGPointMake(200, 400)];
    springAni.duration = springAni.settlingDuration;
    [_mainLayer addAnimation:springAni forKey:@"springAnimation"];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
