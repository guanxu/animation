//
//  CABasicAnimationVC.m
//  animation
//
//  Created by 关旭 on 2019/3/6.
//  Copyright © 2019 guanxu. All rights reserved.
//

#import "CABasicAnimationVC.h"

@interface CABasicAnimationVC ()<CAAnimationDelegate>
@property (nonatomic,strong)CALayer *mainLayer;


@end

@implementation CABasicAnimationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self initView];
}
- (void)initView {
    CALayer *layer = [[CALayer alloc]init];
    layer.bounds = CGRectMake(0, 0, 100, 100);
    layer.position = self.view.center;
    layer.backgroundColor = [UIColor redColor].CGColor;
    [self.view.layer addSublayer:layer];
    
    self.mainLayer = layer;
    
    NSArray *buttonNameArr = @[@"位移",@"缩放",@"透明度",@"旋转",@"圆角"];
    
    for (int i = 0; i < 5; i++) {
        UIButton *aniButton = [UIButton buttonWithType:UIButtonTypeCustom];
        aniButton.tag = i;
        [aniButton setTitle:buttonNameArr[i] forState:UIControlStateNormal];
        aniButton.exclusiveTouch = YES;
        aniButton.frame = CGRectMake(10, 100 + 60 * i, 100, 50);
        aniButton.backgroundColor = [UIColor blueColor];
        [aniButton addTarget:self action:@selector(tapAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:aniButton];
    }
    
    
}
-(void)tapAction:(UIButton*)button{
    CABasicAnimation *basicAni = nil;
    switch (button.tag) {
        case 0:
            //位移动画
            basicAni = [CABasicAnimation animationWithKeyPath:@"position"];
            //到达位置
//            basicAni.byValue = [NSValue valueWithCGPoint:CGPointMake(100, 100)];
            basicAni.toValue = [NSValue valueWithCGPoint:CGPointMake(_mainLayer.position.x+100, _mainLayer.position.y+100)];
            break;
        case 1:
            //缩放动画
            basicAni = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
            //到达缩放
            basicAni.toValue = @(0.1f);
            break;
        case 2:
            //透明度动画
            basicAni = [CABasicAnimation animationWithKeyPath:@"opacity"];
            //透明度
            basicAni.toValue=@(0.1f);
            break;
        case 3:
            //旋转动画
            basicAni = [CABasicAnimation animationWithKeyPath:@"transform"];
            //3D
            basicAni.toValue=[NSValue valueWithCATransform3D:CATransform3DMakeRotation(M_PI_2+M_PI_4, 1, 1, 0)];
            break;
        case 4:
            //圆角动画
            basicAni = [CABasicAnimation animationWithKeyPath:@"cornerRadius"];
            //圆角
            basicAni.toValue=@(50);
            break;
            
        default:
            break;
    }
    basicAni.delegate = self;//设置代理
//    basicAni.beginTime = CACurrentMediaTime()+2;//延时执行
    basicAni.duration = 1;//动画时间
    basicAni.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];//动画节奏
    basicAni.speed = 0.6;//动画速率
    
//    图层是否显示执行后的动画执行后的位置以及状态
    basicAni.removedOnCompletion = NO;
    basicAni.fillMode = kCAFillModeForwards;
    
//    basicAni.autoreverses = YES;//动画执行完后是否以动画的形式回到初始值
//    basicAni.timeOffset = 0.1;//动画时间偏移
    
    [self.mainLayer addAnimation:basicAni forKey:NSStringFromSelector(_cmd)];
}
//暂停动画
-(void)animationPause{
    //获取当前layer的动画媒体时间
    CFTimeInterval interval = [self.mainLayer convertTime:CACurrentMediaTime() toLayer:nil];
    //设置时间偏移量,保证停留在当前位置
    self.mainLayer.timeOffset = interval;
    //暂定动画
    self.mainLayer.speed = 0;
}
//恢复动画
-(void)animationResume{
    //获取暂停的时间
    CFTimeInterval beginTime = CACurrentMediaTime() - self.mainLayer.timeOffset;
    //设置偏移量
    self.mainLayer.timeOffset = 0;
    //设置开始时间
    self.mainLayer.beginTime = beginTime;
    //开始动画
    self.mainLayer.speed = 1;
}
//停止动画
-(void)animationStop{
    [self.mainLayer removeAllAnimations];
    [self.mainLayer removeAnimationForKey:@"groupAnimation"];
}


#pragma mark ------------CAAnimationDelegate
- (void)animationDidStart:(CAAnimation *)anim {
    
}


- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
//    //开始事务
//    [CATransaction begin];
//    //关闭隐式动画
//    [CATransaction setDisableActions:YES];
//    _mainLayer.position = [[anim valueForKey:@"positionToEnd"] CGPointValue];
//    //提交事务
//    [CATransaction commit];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
