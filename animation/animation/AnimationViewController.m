//
//  AnimationViewController.m
//  animation
//
//  Created by 关旭 on 2019/3/5.
//  Copyright © 2019 guanxu. All rights reserved.
//

#import "AnimationViewController.h"

#define SCREEN_W self.view.frame.size.width
#define SCREEN_H self.view.frame.size.height

@interface AnimationViewController ()
@property (nonatomic,strong)UIImageView *showImage;
@property (nonatomic,strong)UIButton *showButton;



@end

@implementation AnimationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self initView];
    
    
}
- (void)initView {
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_W/2-75, SCREEN_H/2-75, 150, 150)];
    imageView.image = [UIImage imageNamed:@"example"];
//    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:imageView];
    
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_W/2-100, SCREEN_H-70, 200, 40)];
    [button setTitle:@"开始动画" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(startAnimation) forControlEvents:UIControlEventTouchUpInside];
    button.layer.borderColor = [UIColor grayColor].CGColor;
    button.layer.cornerRadius = 20;
    button.layer.borderWidth = 1;
    [button setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [self.view addSubview:button];
    
    
    self.showImage = imageView;
    self.showButton = button;
}
- (void)startAnimation {
    switch (self.viewAnimationIndex) {
        case 0://大小动画（frame 改变）
            [self initSizeAnimation];
            break;
        case 1://拉伸动画
            [self initBoundsAnimation];
            break;
        case 2://中心位置动画
            [self initCenterAnimation];
            break;
        case 3://旋转动画(transform)
            [self initTransformAnimation];
            break;
        case 4://透明度动画
            [self initAlphaAnimation];
            break;
        case 5://背景色动画
            [self initBackgroundAnimation];
            break;
        case 6://spring 动画
            [self initSpringAnimation];
            break;
        case 7://转场动画
            [self initTransitionAnimation];
            break;
        default:
            break;
    }
}
#pragma mark ----------------转场动画 transition
- (void)initTransitionAnimation {
    [UIView transitionWithView:self.showImage duration:2.0 options:UIViewAnimationOptionTransitionFlipFromTop animations:^{
        
    } completion:^(BOOL finished) {
        [UIView transitionWithView:self.showImage  duration:2  options:UIViewAnimationOptionTransitionFlipFromBottom animations:^{
            
        } completion:^(BOOL finished) {
            
        }];
    }];
}
#pragma mark ----------------spring 动画（弹簧效果）
- (void)initSpringAnimation {
    CGRect origin = self.showImage.frame;
    CGRect terminal = CGRectMake(origin.origin.x+50, origin.origin.y, 150, 150);
    [UIView animateWithDuration:1 delay:0 usingSpringWithDamping:0.5 initialSpringVelocity:4 options:UIViewAnimationOptionCurveLinear animations:^{
        self.showImage.frame = terminal;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1 delay:1 usingSpringWithDamping:0.5 initialSpringVelocity:4 options:UIViewAnimationOptionCurveLinear animations:^{
            self.showImage.frame = origin;
        } completion:^(BOOL finished) {
            
        }];
    }];
}
#pragma mark ----------------背景颜色动画 改变 background
- (void)initBackgroundAnimation {
    self.showImage.image = [UIImage imageNamed:@"example1"];
    
    [UIView animateKeyframesWithDuration:6.0 delay:0.f options:UIViewKeyframeAnimationOptionCalculationModeLinear animations:^{
        [UIView addKeyframeWithRelativeStartTime:0.f relativeDuration:1.0 / 4 animations:^{
            self.showImage.backgroundColor = [UIColor redColor];
        }];
        [UIView addKeyframeWithRelativeStartTime:1.0 / 4 relativeDuration:1.0 / 4 animations:^{
            self.showImage.backgroundColor = [UIColor greenColor];
        }];
        [UIView addKeyframeWithRelativeStartTime:2.0 / 4 relativeDuration:1.0 / 4 animations:^{
            self.showImage.backgroundColor = [UIColor yellowColor];
        }];
        [UIView addKeyframeWithRelativeStartTime:2.0 / 4 relativeDuration:1.0 / 4 animations:^{
            self.showImage.backgroundColor = [UIColor greenColor];
        }];
        [UIView addKeyframeWithRelativeStartTime:1.0 / 4 relativeDuration:1.0 / 4 animations:^{
            self.showImage.backgroundColor = [UIColor whiteColor];
        }];
    } completion:^(BOOL finished) {
        NSLog(@"动画结束");
    }];
}
#pragma mark ----------------透明度动画 改变alpha
- (void)initAlphaAnimation {
    [UIView animateWithDuration:2 animations:^{
        self.showImage.alpha = 0.3;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:2 animations:^{
            self.showImage.alpha = 1;
        }];
    }];
}



#pragma mark ----------------旋转动画，改变transform
- (void)initTransformAnimation {
    CGAffineTransform origin = self.showImage.transform;
    [UIView animateWithDuration:2 animations:^{
//        self.showImage.transform = CGAffineTransformMakeScale(0.6, 0.6);//缩放
//        self.showImage.transform = CGAffineTransformMakeTranslation(60, -60);//偏移
        self.showImage.transform = CGAffineTransformMakeRotation(4.0f);//旋转
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:2 animations:^{
            self.showImage.transform = origin;
        }];
    }];
}
#pragma mark ----------------中心位置动画，改变center
- (void)initCenterAnimation {
    CGPoint origin = self.showImage.center;
    CGPoint terminal = CGPointMake(self.showImage.center.x, self.showImage.center.y-100);
    [UIView animateWithDuration:1 animations:^{
        self.showImage.center = terminal;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1 animations:^{
            self.showImage.center = origin;
        }];
    }];
}
#pragma mark ------------------拉伸动画 bounds 改变
- (void)initBoundsAnimation {
    CGRect origin = self.showImage.bounds;
    //拉伸动画基于view的bound改变，只改变宽高，
    CGRect terminal = CGRectMake(0, 0, 200, 150);
    [UIView animateWithDuration:1 animations:^{
        self.showImage.bounds = terminal;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1 animations:^{
            self.showImage.bounds = origin;
        }];
    }];
}
#pragma mark  ---------------------大小动画，frame 改变
- (void)initSizeAnimation {
    CGRect origin = self.showImage.frame;
    CGRect terminal = CGRectMake(SCREEN_W/2-100, SCREEN_H/2-100, 200, 200);
    [UIView animateWithDuration:1 animations:^{
        self.showImage.frame = terminal;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1 animations:^{
            self.showImage.frame = origin;
        }];
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
