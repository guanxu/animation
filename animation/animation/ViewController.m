//
//  ViewController.m
//  animation
//
//  Created by 关旭 on 2019/3/4.
//  Copyright © 2019 guanxu. All rights reserved.
//

#import "ViewController.h"
#import "AnimationViewController.h"
#import "CABasicAnimationVC.h"
#import "CAAnimationVC.h"



@interface ViewController ()<UITableViewDelegate,UITableViewDataSource>{
    NSArray *animationArr;
}
@property (nonatomic,strong)UITableView *tableView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    animationArr = [[NSArray alloc]init];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"动画";
    
    NSArray *viewAnimation = @[
                               @"大小动画(frame改变)",
                               @"拉伸动画（改变bounds）",
                               @"中心位置动画（center）",
                               @"旋转动画（transform）",
                               @"透明变化（alpha）",
                               @"背景颜色变化",
                               @"Spring（弹簧）动画",
                               @"转场动画（transition）"
                               ];
    NSArray *coreAnimation = @[
                               @"CABasicAnimation",
                               @"CASpringAnimation",
                               @"CAKeyframeAnimation（晃动）",
                               @"CAKeyframeAnimation（曲线位移）",
                               @"CATransition",
                               @"CAAnimationGroup"
                               ];
    
    animationArr = @[viewAnimation,coreAnimation];
    
    [self tableView];
    
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [self.view addSubview:_tableView];
    }
    return _tableView;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return animationArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *array = [animationArr[section] copy];
    return array.count;
}
- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return @"UIView Animation";
    }else{
        return @"Core Animation";
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellID = @"cellID";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    NSArray *array = [animationArr[indexPath.section] copy];
    cell.textLabel.text = array[indexPath.row];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *arr = [animationArr[indexPath.section] copy];
    if (indexPath.section == 0) {
        AnimationViewController *view = [[AnimationViewController alloc]init];
        view.viewAnimationIndex = indexPath.row;
        view.title = arr[indexPath.row];
        [self.navigationController pushViewController:view animated:YES];
    }else{
        if(indexPath.row == 0){
            CABasicAnimationVC *view = [[CABasicAnimationVC alloc]init];
            view.title = arr[indexPath.row];
            [self.navigationController pushViewController:view animated:YES];
        }else{
            CAAnimationVC *view = [[CAAnimationVC alloc]init];
            view.title = arr[indexPath.row];
            view.animationIndex = indexPath.row;
            [self.navigationController pushViewController:view animated:YES];
        }
    }
    
    
}
@end
